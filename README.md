# Debian Workstation

## Installation

## Install Ansible

```bash
wget -O- "https://keyserver.ubuntu.com/pks/lookup?fingerprint=on&op=get&search=0x6125E2A8C77F2818FB7BD15B93C4A3FD7BB9C367" | gpg --dearmour -o /usr/share/keyrings/ansible-archive-keyring.gpg
echo "deb [signed-by=/usr/share/keyrings/ansible-archive-keyring.gpg] http://ppa.launchpad.net/ansible/ansible/ubuntu jammy main" | tee /etc/apt/sources.list.d/ansible.list
apt update && apt install -y ansible
```

# todo

- https://vscodium.com/#install
- 
## pdf arranger
```bash
apt install pdfarranger
```

## vm machines
- install spice-vdagent

## Docs
- https://github.com/linuxmint/cinnamon/wiki/Backing-up-and-restoring-your-cinnamon-settings-(dconf)

## Installation

### Basic

```bash
apt install -y openssh-server net-tools htop mc git 
```

### Desktop

```bash
apt install -y cinnamon-core lightdm-gtk-greeter xorg xinit synaptic
```

### Theme
```bash
wget https://cinnamon-spices.linuxmint.com/files/themes/Adapta-Nokto.zip
unzip Adapta-Nokto.zip
mv Adapta-Nokto /usr/share/themes

sudo -u christian dbus-launch gsettings set org.cinnamon.theme name "Adapta-Nokto" && \
sudo -u christian dbus-launch gsettings set org.cinnamon.desktop.interface gtk-theme "Adapta-Nokto" && \
sudo -u christian dbus-launch gsettings set org.cinnamon.desktop.wm.preferences theme "Adapta-Nokto" && \
sudo -u christian dbus-launch gsettings set org.cinnamon.desktop.interface icon-theme "Papirus"

mkdir -p /home/christian/Bilder/wallpaper
wget https://images6.alphacoders.com/712/712437.jpg -O /home/christian/Bilder/wallpaper/deer-blue.jpg
sudo -u christian dbus-launch gsettings set org.cinnamon.desktop.background picture-uri 'file:///home/christian/Bilder/wallpaper/deer-blue.jpg'

```


## Configuration

### User to sudo

After that command we need a reboot, to enable sudo for the user

```bash
su -
usermod -aG sudo christian
```



## Themes
- https://cinnamon-spices.linuxmint.com/themes/view/Obsidian
- https://www.cinnamon-look.org/p/1501103
- https://www.cinnamon-look.org/p/1683760

## Icons
- https://github.com/PapirusDevelopmentTeam/papirus-icon-theme?tab=readme-ov-file

## Wallpaper
- https://wall.alphacoders.com/big.php?i=712437


